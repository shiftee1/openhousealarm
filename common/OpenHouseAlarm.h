/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm definitions header file
 */
#pragma once

#include <stdint.h>

const int32_t  SYS_NUM_INPUTS         = 12;
const int32_t  SYS_NUM_OUTPUTS        = 12;
const int32_t  SYS_NUM_MODES          = 12;
const int32_t  SYS_PANIC_DELAY_MAX    = 45;
const int32_t  SYS_ARMING_DELAY_MIN   = 10;
const int32_t  SYS_ARMING_DELAY_MAX   = 120;
const uint32_t SYS_AUTH_TIMEOUT       = 60;
const uint8_t  SYS_MODBUS_SLAVE_ID    = 7;

enum modbus_reg_t {                               //modbus registers
	MODBUS_REG_ARMED,
	MODBUS_REG_DELAYED_ARMED,
	MODBUS_REG_PANIC,
	MODBUS_REG_DELAYED_PANIC,
	MODBUS_REG_MODE,
	MODBUS_REG_MODE_MASK,
	MODBUS_REG_AUTH,
	MODBUS_REG_PIN,
	MODBUS_REG_INPUT_01_TYPE,     MODBUS_REG_INPUT_02_TYPE,     MODBUS_REG_INPUT_03_TYPE,
	MODBUS_REG_INPUT_04_TYPE,     MODBUS_REG_INPUT_05_TYPE,     MODBUS_REG_INPUT_06_TYPE,
	MODBUS_REG_INPUT_07_TYPE,     MODBUS_REG_INPUT_08_TYPE,     MODBUS_REG_INPUT_09_TYPE,
	MODBUS_REG_INPUT_10_TYPE,     MODBUS_REG_INPUT_11_TYPE,     MODBUS_REG_INPUT_12_TYPE,
	MODBUS_REG_INPUT_01_DELAY,    MODBUS_REG_INPUT_02_DELAY,    MODBUS_REG_INPUT_03_DELAY,
	MODBUS_REG_INPUT_04_DELAY,    MODBUS_REG_INPUT_05_DELAY,    MODBUS_REG_INPUT_06_DELAY,
	MODBUS_REG_INPUT_07_DELAY,    MODBUS_REG_INPUT_08_DELAY,    MODBUS_REG_INPUT_09_DELAY,
	MODBUS_REG_INPUT_10_DELAY,    MODBUS_REG_INPUT_11_DELAY,    MODBUS_REG_INPUT_12_DELAY,
	MODBUS_REG_INPUT_01_ENABLE,   MODBUS_REG_INPUT_02_ENABLE,   MODBUS_REG_INPUT_03_ENABLE,
	MODBUS_REG_INPUT_04_ENABLE,   MODBUS_REG_INPUT_05_ENABLE,   MODBUS_REG_INPUT_06_ENABLE,
	MODBUS_REG_INPUT_07_ENABLE,   MODBUS_REG_INPUT_08_ENABLE,   MODBUS_REG_INPUT_09_ENABLE,
	MODBUS_REG_INPUT_10_ENABLE,   MODBUS_REG_INPUT_11_ENABLE,   MODBUS_REG_INPUT_12_ENABLE,
	MODBUS_REG_OUTPUT_01_TYPE,    MODBUS_REG_OUTPUT_02_TYPE,    MODBUS_REG_OUTPUT_03_TYPE,
	MODBUS_REG_OUTPUT_04_TYPE,    MODBUS_REG_OUTPUT_05_TYPE,    MODBUS_REG_OUTPUT_06_TYPE,
	MODBUS_REG_OUTPUT_07_TYPE,    MODBUS_REG_OUTPUT_08_TYPE,    MODBUS_REG_OUTPUT_09_TYPE,
	MODBUS_REG_OUTPUT_10_TYPE,    MODBUS_REG_OUTPUT_11_TYPE,    MODBUS_REG_OUTPUT_12_TYPE,
	MODBUS_REG_OUTPUT_01_PULSE,   MODBUS_REG_OUTPUT_02_PULSE,   MODBUS_REG_OUTPUT_03_PULSE,
	MODBUS_REG_OUTPUT_04_PULSE,   MODBUS_REG_OUTPUT_05_PULSE,   MODBUS_REG_OUTPUT_06_PULSE,
	MODBUS_REG_OUTPUT_07_PULSE,   MODBUS_REG_OUTPUT_08_PULSE,   MODBUS_REG_OUTPUT_09_PULSE,
	MODBUS_REG_OUTPUT_10_PULSE,   MODBUS_REG_OUTPUT_11_PULSE,   MODBUS_REG_OUTPUT_12_PULSE,
	MODBUS_REG_OUTPUT_01_ENABLE,  MODBUS_REG_OUTPUT_02_ENABLE,  MODBUS_REG_OUTPUT_03_ENABLE,
	MODBUS_REG_OUTPUT_04_ENABLE,  MODBUS_REG_OUTPUT_05_ENABLE,  MODBUS_REG_OUTPUT_06_ENABLE,
	MODBUS_REG_OUTPUT_07_ENABLE,  MODBUS_REG_OUTPUT_08_ENABLE,  MODBUS_REG_OUTPUT_09_ENABLE,
	MODBUS_REG_OUTPUT_10_ENABLE,  MODBUS_REG_OUTPUT_11_ENABLE,  MODBUS_REG_OUTPUT_12_ENABLE,
	MODBUS_REG_MODE_01_MASK,      MODBUS_REG_MODE_02_MASK,      MODBUS_REG_MODE_03_MASK,
	MODBUS_REG_MODE_04_MASK,      MODBUS_REG_MODE_05_MASK,      MODBUS_REG_MODE_06_MASK,
	MODBUS_REG_MODE_07_MASK,      MODBUS_REG_MODE_08_MASK,      MODBUS_REG_MODE_09_MASK,
	MODBUS_REG_MODE_10_MASK,      MODBUS_REG_MODE_11_MASK,      MODBUS_REG_MODE_12_MASK,
	MODBUS_REG_MODE_01_ARM_DELAY, MODBUS_REG_MODE_02_ARM_DELAY, MODBUS_REG_MODE_03_ARM_DELAY,
	MODBUS_REG_MODE_04_ARM_DELAY, MODBUS_REG_MODE_05_ARM_DELAY, MODBUS_REG_MODE_06_ARM_DELAY,
	MODBUS_REG_MODE_07_ARM_DELAY, MODBUS_REG_MODE_08_ARM_DELAY, MODBUS_REG_MODE_09_ARM_DELAY,
	MODBUS_REG_MODE_10_ARM_DELAY, MODBUS_REG_MODE_11_ARM_DELAY, MODBUS_REG_MODE_12_ARM_DELAY,
	MODBUS_NUM_REGS
};

enum modbus_exception_t {                         //modbus exception codes
	MODBUS_EXCEPTION_BAD_FUNC = 1,
	MODBUS_EXCEPTION_BAD_ADDR,
	MODBUS_EXCEPTION_BAD_VALUE,
	MODBUS_EXCEPTION_FAILURE,
	MODBUS_NUM_EXCEPTIONS
};

enum input_type_t {                               //alarm input types
	INPUT_SENSOR_MOTION,                              //motion sensor
	INPUT_SENSOR_CONTACT_DOOR,                        //door contact sensor
	INPUT_SENSOR_CONTACT_WINDOW,                      //window contact sensor
	INPUT_SIGNAL,                                     //smoke-alarm, panic-button etc.
	INPUT_NUM_TYPES
};

enum output_type_t {                              //alarm output types
	OUTPUT_ARMED,                                     //alarm armed state
	OUTPUT_PANIC,                                     //alarm panic state
	OUTPUT_DELAYED_ARMED,                             //alarm delayed-armed state
	OUTPUT_DELAYED_PANIC,                             //alarm delayed-panic state
	OUTPUT_NUM_TYPES
};

struct alarm_mode_t                               //alarm mode struct
{
	uint16_t        mask;                             //bitmask to define enabled inputs
	bool            armed_delay_enable;               //arming delay enabled/disabled for mode
};

struct input_cnfg_t                               //input config struct
{
	input_type_t    type;                             //input type
	uint16_t        delay;                            //delay before alarm activation in seconds
	bool            enabled;                          //input enable/disable status
};

struct output_cnfg_t                              //output config struct
{
	output_type_t   type;                             //output type
	bool            pulse;                            //output pulses when enabled
	bool            enabled;                          //output enable/disable status
};

struct config_t                                   //system config struct
{
	uint16_t        config_version;                   //configuration format version
	input_cnfg_t    input[SYS_NUM_INPUTS];            //system input configs
	output_cnfg_t   output[SYS_NUM_OUTPUTS];          //system output configs
	alarm_mode_t    mode[SYS_NUM_MODES];              //system alarm modes
	uint16_t        armed_delay;                      //delay in seconds when arming system
	uint16_t        pin;                              //user pin
};

struct state_t                                    //system state struct
{
	bool       armed;                                 //system armed state
	uint32_t   armed_delay_start;                     //start milliseconds of current armed delay
	uint32_t   armed_delay_count;                     //number of seconds in current armed delay
	bool       panic;                                 //system panic state
	uint32_t   panic_delay_start;                     //start milliseconds of current panic delay
	uint32_t   panic_delay_count;                     //number of seconds in current panic delay
	uint32_t   panic_start;                           //start time of a system panic
	uint16_t   alarm_mode;                            //alarm operation mode
	uint16_t   alarm_mask;                            //alarm operation mask
	bool       auth;                                  //user authentication status
	uint32_t   auth_tm;                               //time of last user authentication
	int        input_val[SYS_NUM_INPUTS];             //current system input values
	int        input_val_prev[SYS_NUM_INPUTS];        //previous system input values
};
