/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm I/O module
 *
 * Currently designed to run on Controllino Maxi
 */

#include <Controllino.h>
#include <EEPROM.h>

#include "common/OpenHouseAlarm.h"

const uint16_t SYS_CONFIG_VERSION          = 1;   //config format version


const int sys_inputs[SYS_NUM_INPUTS] = {
	CONTROLLINO_A0,
	CONTROLLINO_A1,
	CONTROLLINO_A2,
	CONTROLLINO_A3,
	CONTROLLINO_A4,
	CONTROLLINO_A5,
	CONTROLLINO_A6,
	CONTROLLINO_A7,
	CONTROLLINO_A8,
	CONTROLLINO_A9,
	CONTROLLINO_IN0,
	CONTROLLINO_IN1
};

const int sys_outputs[SYS_NUM_OUTPUTS] = {
	CONTROLLINO_D0,
	CONTROLLINO_D1,
	CONTROLLINO_D2,
	CONTROLLINO_D3,
	CONTROLLINO_D4,
	CONTROLLINO_D5,
	CONTROLLINO_D6,
	CONTROLLINO_D7,
	CONTROLLINO_D8,
	CONTROLLINO_D9,
	CONTROLLINO_D10,
	CONTROLLINO_D11
};


//global variables
state_t    state;                                 //system state
config_t   config;                                //system configuration


/*
 * Read a byte from I2C EEPROM
 *     reg: register address
 */
uint8_t eepromReadByte(uint8_t reg)
{
	return EEPROM.read(reg);
}

/*
 * Write a byte to I2C EEPROM
 *     reg: register address
 *     val: value to write
 *
 * Returns 0 on success
 */
int eepromWriteByte(uint8_t reg, uint8_t val)
{
	int status = 1;

	if( reg < EEPROM.length() )
	{
		EEPROM.update(reg, val);
		status = 0;
	}

	return status;
}

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t getCRC16(uint8_t* buf, unsigned num_bytes)
{
	uint16_t crc = 0xFFFF;

	for(unsigned i=0; i<num_bytes; i++)
	{
		crc ^= buf[i];

		for(int j=0; j<8; j++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ 0xA001;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Setup the RS485 port
 */
void setupRS485(unsigned baud)
{
	Controllino_RS485Init(baud);
	Controllino_RS485RxEnable();
}

/*
 * Read up to max_bytes bytes into buf from the RS485 port
 *
 * Returns the number of bytes read
 */
unsigned readRS485(uint8_t* buf, unsigned max_bytes)
{
	unsigned bytes_read = 0;

	auto& port = Serial3;

	while( port.available() )
	{
		buf[bytes_read++] = port.read();

		if( bytes_read == max_bytes )
			break;

		//bytes may still be coming in
		if( ! port.available() )
			delay(2);
	}

	return bytes_read;
}

/*
 * Send num_bytes from buf on the RS485 port
 *
 * Returns the number of bytes sent
 */
unsigned sendRS485(uint8_t* buf, unsigned num_bytes)
{
	Controllino_RS485TxEnable();

	unsigned bytes_sent = Serial3.write(buf, num_bytes);

	Serial3.flush();

	Controllino_RS485RxEnable();

	return bytes_sent;
}

/*
 * Sets the default configuration
 */
void setDefaultConfig()
{
	config.config_version = SYS_CONFIG_VERSION;

	//configure inputs
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		config.input[i].type    = INPUT_SENSOR_MOTION;
		config.input[i].delay   = 0;
		config.input[i].enabled = false;
	}
	config.input[0].type      = INPUT_SENSOR_CONTACT_DOOR;
	config.input[0].delay     = 30;            //door sensor
	config.input[0].enabled   = true;
	config.input[10].type     = INPUT_SIGNAL;  //panic button
	config.input[10].enabled  = true;
	config.input[11].type     = INPUT_SIGNAL;  //smoke alarm
	config.input[11].enabled  = true;

	//configure outputs
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
	{
		config.output[i].type    = OUTPUT_ARMED;
		config.output[i].pulse   = false;
		config.output[i].enabled = true;
	}
	config.output[2].type     = OUTPUT_PANIC;
	config.output[3].type     = OUTPUT_PANIC;
	config.output[4].type     = OUTPUT_DELAYED_ARMED;
	config.output[5].type     = OUTPUT_DELAYED_ARMED;
	config.output[6].type     = OUTPUT_DELAYED_PANIC;
	config.output[7].type     = OUTPUT_DELAYED_PANIC;

	config.output[8].type     = OUTPUT_ARMED;
	config.output[9].type     = OUTPUT_PANIC;
	config.output[10].type    = OUTPUT_DELAYED_ARMED;
	config.output[11].type    = OUTPUT_DELAYED_PANIC;

	config.output[8].pulse    = true;
	config.output[9].pulse    = true;
	config.output[10].pulse   = true;
	config.output[11].pulse   = true;

	//configure alarm modes
	for(int i=0; i<SYS_NUM_MODES; i++)
	{
		config.mode[i].mask               = 0;
		config.mode[i].armed_delay_enable = true;
	}
	config.mode[0].mask = 0xFFFF;

	//configure the arming delay
	config.armed_delay = 45;

	//configure the default pincode
	config.pin = 0;
}

/*
 * Save the system configuration to EEPROM
 *
 * Returns 0 on success
 */
int saveConfig()
{
	unsigned num_bytes = sizeof(config_t);

	uint8_t* p = (uint8_t*) &config;

	for(unsigned i=0; i<num_bytes; i++)
	{
		uint8_t val = *(p+i);

		eepromWriteByte(i, val);
	}

	uint16_t crc = getCRC16(p, num_bytes);

	int status = 0;

	status += eepromWriteByte(num_bytes++, lowByte(crc));
	status += eepromWriteByte(num_bytes++, highByte(crc));

	return status;
}

/*
 * Loads the system configuration from EEPROM
 *
 * Returns 0 on success
 */
int loadConfig()
{
	unsigned num_bytes = sizeof(config_t);

	uint8_t* p = (uint8_t*) &config;

	for(unsigned i=0; i<num_bytes; i++)
	{
		p[i] = eepromReadByte(i);
	}

	uint16_t real_crc = getCRC16(p, num_bytes);

	uint8_t buf[2];
	buf[0] = eepromReadByte(num_bytes++);
	buf[1] = eepromReadByte(num_bytes++);

	uint16_t crc = buf[1] << 8 | buf[0];

	int status = 0;

	if( crc != real_crc )
	{
		setDefaultConfig();

		saveConfig();

		status = -1;
	}

	return status;
}

/*
 * Setup the digital inputs
 */
void setupInputs()
{
	for(int i=0; i<SYS_NUM_INPUTS; i++)
		pinMode(sys_inputs[i], INPUT);
}

/*
 * Setup the digital outputs
 */
void setupOutputs()
{
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
		pinMode(sys_outputs[i], OUTPUT);
}

/*
 * Read the digital input at the supplied index
 *
 * Returns 0 on failure
 */
int readInput(unsigned index)
{
	int val = 0;

	if( index < SYS_NUM_INPUTS )
		val = digitalRead( sys_inputs[index] );

	return val;
}

/*
 * Read all the digital inputs
 */
void readAllInputs()
{
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		state.input_val_prev[i] = state.input_val[i];

		state.input_val[i]      = readInput(i);
	}
}

/*
 * Set the state of a digital output
 */
void setOutput(unsigned index, bool val)
{
	if( index < SYS_NUM_OUTPUTS )
		digitalWrite( sys_outputs[index], val );
}

/*
 * Reset the user pincode to default values
 *
 * Returns 0 on success
 */
int resetUserPincode()
{
	config.pin = 0;

	return saveConfig();
}

/*
 * Set the system panic state
 */
void setPanicState(bool val)
{
	if( val && ! state.panic )
		state.panic_start = millis();

	state.panic = val;
}

/*
 * Returns true if a window is open
 */
bool isAnyWindowOpen()
{
	bool window_open = false;

	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		if( config.input[i].type == INPUT_SENSOR_CONTACT_WINDOW )
		{
			if( state.input_val[i] == 0 )
				window_open = true;
		}
	}

	return window_open;
}

/*
 * Returns true if the passed alarm mode is valid and usable
 */
bool isModeValid(unsigned mode)
{
	bool mode_valid = false;

	if( mode < SYS_NUM_MODES )
	{
		mode_valid = config.mode[mode].mask > 0;
	}

	return mode_valid;
}

/*
 * Returns true if the alarm is armed or arming
 */
bool isSystemArmed()
{
	return state.armed || state.armed_delay_count;
}

/*
 * Disarm the alarm system
 */
void disarmSystem()
{
	state.armed             = false;
	state.armed_delay_count = 0;
	state.panic             = false;
	state.panic_delay_count = 0;
}

/*
 * Arms the alarm system
 */
void armSystem(unsigned mode)
{
	bool enable_alarm = true;

	if( state.panic || isAnyWindowOpen() )
		enable_alarm = false;

	if( ! isModeValid(mode) )
		enable_alarm = false;

	if( enable_alarm )
	{
		state.alarm_mode = mode;
		state.alarm_mask = config.mode[state.alarm_mode].mask;

		if( config.mode[state.alarm_mode].armed_delay_enable )
		{
			state.armed_delay_start = millis();
			state.armed_delay_count = config.armed_delay;
		}
		else
		{
			state.armed = true;
		}
	}

	state.panic             = false;
	state.panic_delay_count = 0;
}

/*
 * Formats the data in src buffer as a hex string
 *
 * Returns the number of chars
 */
int bufToHex(char* dest, uint8_t* src, unsigned num_bytes)
{
	//null terminate if num_bytes == 0
	dest[0] = 0;

	int chars = 0;

	for(unsigned i=0; i<num_bytes; i++)
		chars += sprintf(dest+i*2, "%02X", src[i]);

	return chars;
}

/*
 * Print a modbus packet to the serial port
 */
void printModbusDebugInfo(const char* label, uint8_t* buf, unsigned num_bytes)
{
	if( num_bytes > 30 )
		num_bytes = 30;

	unsigned buf_size = (num_bytes*2) + 1;

	char str[buf_size];

	auto num_chars = bufToHex(str, buf, num_bytes);

	Serial.print(label);
	Serial.write(str, num_chars);
	Serial.println("");
}

/*
 * Prepare a modbus exception response
 *
 * Returns the response packet size in bytes
 */
int handleModbusException(uint8_t* buf, modbus_exception_t exception)
{
		buf[1] |= 0x80;
		buf[2] = exception;

		return 3;
}

/*
 * Prepare a modbus response packet in buf for modbus function 3
 * Ignores the first two bytes in the buffer and doesn't handle the CRC
 *
 * Returns the response packet size in bytes
 */
unsigned handleModbusFunc3(uint8_t* buf, uint16_t offset)
{
	unsigned num_bytes = 0;

	unsigned count = buf[4] << 8 | buf[5];

	//check parameters
	bool bad_addr  = offset >= MODBUS_NUM_REGS;

	bool bad_count = count < 1 || count > MODBUS_NUM_REGS;

	int last_reg = offset + count - 1;

	if( bad_addr || bad_count || last_reg >= MODBUS_NUM_REGS )
		return handleModbusException(buf, MODBUS_EXCEPTION_BAD_ADDR);

	//build response
	buf[2] = count * 2;

	num_bytes = 3;

	for(unsigned i=0; i<count; i++)
	{
		uint16_t val = 0;

		switch( static_cast<modbus_reg_t>(offset) )
		{
		case MODBUS_REG_ARMED:
			val = state.armed;
			break;
		case MODBUS_REG_DELAYED_ARMED:
			val = state.armed_delay_count;
			break;
		case MODBUS_REG_PANIC:
			val = state.panic;
			break;
		case MODBUS_REG_DELAYED_PANIC:
			val = state.panic_delay_count;
			break;
		case MODBUS_REG_MODE:
			val = state.alarm_mode;
			break;
		case MODBUS_REG_MODE_MASK:
			val = state.alarm_mask;
			break;
		case MODBUS_REG_PIN:
			//return 0 if pincode is not set, else return 0xFFFF
			if( config.pin == 0 )
				val = 0;
			else
				val = 0xFFFF;
			break;
		case MODBUS_REG_AUTH:
			val = state.auth;
			break;
		case MODBUS_REG_INPUT_01_TYPE:
		case MODBUS_REG_INPUT_02_TYPE:
		case MODBUS_REG_INPUT_03_TYPE:
		case MODBUS_REG_INPUT_04_TYPE:
		case MODBUS_REG_INPUT_05_TYPE:
		case MODBUS_REG_INPUT_06_TYPE:
		case MODBUS_REG_INPUT_07_TYPE:
		case MODBUS_REG_INPUT_08_TYPE:
		case MODBUS_REG_INPUT_09_TYPE:
		case MODBUS_REG_INPUT_10_TYPE:
		case MODBUS_REG_INPUT_11_TYPE:
		case MODBUS_REG_INPUT_12_TYPE:
		{
			auto j = offset - MODBUS_REG_INPUT_01_TYPE;
			val = config.input[j].type;
			break;
		}
		case MODBUS_REG_INPUT_01_DELAY:
		case MODBUS_REG_INPUT_02_DELAY:
		case MODBUS_REG_INPUT_03_DELAY:
		case MODBUS_REG_INPUT_04_DELAY:
		case MODBUS_REG_INPUT_05_DELAY:
		case MODBUS_REG_INPUT_06_DELAY:
		case MODBUS_REG_INPUT_07_DELAY:
		case MODBUS_REG_INPUT_08_DELAY:
		case MODBUS_REG_INPUT_09_DELAY:
		case MODBUS_REG_INPUT_10_DELAY:
		case MODBUS_REG_INPUT_11_DELAY:
		case MODBUS_REG_INPUT_12_DELAY:
		{
			auto j = offset - MODBUS_REG_INPUT_01_DELAY;
			val = config.input[j].delay;
			break;
		}
		case MODBUS_REG_INPUT_01_ENABLE:
		case MODBUS_REG_INPUT_02_ENABLE:
		case MODBUS_REG_INPUT_03_ENABLE:
		case MODBUS_REG_INPUT_04_ENABLE:
		case MODBUS_REG_INPUT_05_ENABLE:
		case MODBUS_REG_INPUT_06_ENABLE:
		case MODBUS_REG_INPUT_07_ENABLE:
		case MODBUS_REG_INPUT_08_ENABLE:
		case MODBUS_REG_INPUT_09_ENABLE:
		case MODBUS_REG_INPUT_10_ENABLE:
		case MODBUS_REG_INPUT_11_ENABLE:
		case MODBUS_REG_INPUT_12_ENABLE:
		{
			auto j = offset - MODBUS_REG_INPUT_01_ENABLE;
			val = config.input[j].enabled;
			break;
		}
		case MODBUS_REG_OUTPUT_01_TYPE:
		case MODBUS_REG_OUTPUT_02_TYPE:
		case MODBUS_REG_OUTPUT_03_TYPE:
		case MODBUS_REG_OUTPUT_04_TYPE:
		case MODBUS_REG_OUTPUT_05_TYPE:
		case MODBUS_REG_OUTPUT_06_TYPE:
		case MODBUS_REG_OUTPUT_07_TYPE:
		case MODBUS_REG_OUTPUT_08_TYPE:
		case MODBUS_REG_OUTPUT_09_TYPE:
		case MODBUS_REG_OUTPUT_10_TYPE:
		case MODBUS_REG_OUTPUT_11_TYPE:
		case MODBUS_REG_OUTPUT_12_TYPE:
		{
			auto j = offset - MODBUS_REG_OUTPUT_01_TYPE;
			val = config.output[j].type;
			break;
		}
		case MODBUS_REG_OUTPUT_01_PULSE:
		case MODBUS_REG_OUTPUT_02_PULSE:
		case MODBUS_REG_OUTPUT_03_PULSE:
		case MODBUS_REG_OUTPUT_04_PULSE:
		case MODBUS_REG_OUTPUT_05_PULSE:
		case MODBUS_REG_OUTPUT_06_PULSE:
		case MODBUS_REG_OUTPUT_07_PULSE:
		case MODBUS_REG_OUTPUT_08_PULSE:
		case MODBUS_REG_OUTPUT_09_PULSE:
		case MODBUS_REG_OUTPUT_10_PULSE:
		case MODBUS_REG_OUTPUT_11_PULSE:
		case MODBUS_REG_OUTPUT_12_PULSE:
		{
			auto j = offset - MODBUS_REG_OUTPUT_01_PULSE;
			val = config.output[j].pulse;
			break;
		}
		case MODBUS_REG_OUTPUT_01_ENABLE:
		case MODBUS_REG_OUTPUT_02_ENABLE:
		case MODBUS_REG_OUTPUT_03_ENABLE:
		case MODBUS_REG_OUTPUT_04_ENABLE:
		case MODBUS_REG_OUTPUT_05_ENABLE:
		case MODBUS_REG_OUTPUT_06_ENABLE:
		case MODBUS_REG_OUTPUT_07_ENABLE:
		case MODBUS_REG_OUTPUT_08_ENABLE:
		case MODBUS_REG_OUTPUT_09_ENABLE:
		case MODBUS_REG_OUTPUT_10_ENABLE:
		case MODBUS_REG_OUTPUT_11_ENABLE:
		case MODBUS_REG_OUTPUT_12_ENABLE:
		{
			auto j = offset - MODBUS_REG_OUTPUT_01_ENABLE;
			val = config.output[j].enabled;
			break;
		}
		case MODBUS_REG_MODE_01_MASK:
		case MODBUS_REG_MODE_02_MASK:
		case MODBUS_REG_MODE_03_MASK:
		case MODBUS_REG_MODE_04_MASK:
		case MODBUS_REG_MODE_05_MASK:
		case MODBUS_REG_MODE_06_MASK:
		case MODBUS_REG_MODE_07_MASK:
		case MODBUS_REG_MODE_08_MASK:
		case MODBUS_REG_MODE_09_MASK:
		case MODBUS_REG_MODE_10_MASK:
		case MODBUS_REG_MODE_11_MASK:
		case MODBUS_REG_MODE_12_MASK:
		{
			auto j = offset - MODBUS_REG_MODE_01_MASK;
			val = config.mode[j].mask;
			break;
		}
		case MODBUS_REG_MODE_01_ARM_DELAY:
		case MODBUS_REG_MODE_02_ARM_DELAY:
		case MODBUS_REG_MODE_03_ARM_DELAY:
		case MODBUS_REG_MODE_04_ARM_DELAY:
		case MODBUS_REG_MODE_05_ARM_DELAY:
		case MODBUS_REG_MODE_06_ARM_DELAY:
		case MODBUS_REG_MODE_07_ARM_DELAY:
		case MODBUS_REG_MODE_08_ARM_DELAY:
		case MODBUS_REG_MODE_09_ARM_DELAY:
		case MODBUS_REG_MODE_10_ARM_DELAY:
		case MODBUS_REG_MODE_11_ARM_DELAY:
		case MODBUS_REG_MODE_12_ARM_DELAY:
		{
			auto j = offset - MODBUS_REG_MODE_01_ARM_DELAY;
			val = config.mode[j].armed_delay_enable;
			break;
		}
		case MODBUS_NUM_REGS:
			break;
		}

		buf[num_bytes++] = highByte(val);
		buf[num_bytes++] = lowByte(val);

		offset++;
	}

	return num_bytes;
}

/*
 * Prepare a modbus response packet in buf for modbus function 6
 * Ignores the first two bytes in the buffer and doesn't handle the CRC
 *
 * Returns the response packet size in bytes
 */
unsigned handleModbusFunc6(uint8_t* buf, uint16_t offset)
{
	unsigned num_bytes = 0;

	uint16_t val = buf[4] << 8 | buf[5];

	int status = MODBUS_EXCEPTION_BAD_FUNC;

	if( offset >= MODBUS_NUM_REGS )
		status = MODBUS_EXCEPTION_BAD_ADDR;

	//if not authenticated we can only write to auth register
	if( offset != MODBUS_REG_AUTH )
		if( state.auth == false )
			status = MODBUS_EXCEPTION_BAD_FUNC;

	switch( static_cast<modbus_reg_t>(offset) )
	{
	case MODBUS_REG_ARMED:
		if( isSystemArmed() )
		{
			disarmSystem();
		}
		else
		{
			armSystem(val);
		}

		if( isSystemArmed() )
			Serial.println("system arming");
		else
			Serial.println("system disarmed");

		status = 0;
		break;
	case MODBUS_REG_DELAYED_ARMED:
		if( val > SYS_ARMING_DELAY_MIN && val <= SYS_ARMING_DELAY_MAX )
		{
			state.armed_delay_count = val;
			status = 0;
		}
		break;
	case MODBUS_REG_PANIC:
		setPanicState(val);
		status = 0;
		break;
	case MODBUS_REG_DELAYED_PANIC:
		if( val <= SYS_PANIC_DELAY_MAX )
		{
			state.panic_delay_count = val;
			status = 0;
		}
		break;
	case MODBUS_REG_MODE:
		if( ! state.armed )
		{
			if( val < SYS_NUM_MODES )
			{
				state.alarm_mode = val;
				state.alarm_mask = config.mode[state.alarm_mode].mask;

				status = 0;
			}
		}
		break;
	case MODBUS_REG_MODE_MASK:
		if( ! state.armed )
		{
			state.alarm_mask = val;

			status = 0;
		}
		break;
	case MODBUS_REG_PIN:
		if( state.auth )
		{
			config.pin = val;

			status = saveConfig();
		}
		break;
	case MODBUS_REG_AUTH:
		if( config.pin == val )
		{
			state.auth_tm = millis();
			state.auth    = true;

			disarmSystem();

			status        = 0;
		}
		break;
	case MODBUS_REG_INPUT_01_TYPE:
	case MODBUS_REG_INPUT_02_TYPE:
	case MODBUS_REG_INPUT_03_TYPE:
	case MODBUS_REG_INPUT_04_TYPE:
	case MODBUS_REG_INPUT_05_TYPE:
	case MODBUS_REG_INPUT_06_TYPE:
	case MODBUS_REG_INPUT_07_TYPE:
	case MODBUS_REG_INPUT_08_TYPE:
	case MODBUS_REG_INPUT_09_TYPE:
	case MODBUS_REG_INPUT_10_TYPE:
	case MODBUS_REG_INPUT_11_TYPE:
	case MODBUS_REG_INPUT_12_TYPE:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_INPUT_01_TYPE;

			if( val < INPUT_NUM_TYPES )
			{
				config.input[j].type = static_cast<input_type_t>(val);

				status = saveConfig();
			}
		}
		break;
	case MODBUS_REG_INPUT_01_DELAY:
	case MODBUS_REG_INPUT_02_DELAY:
	case MODBUS_REG_INPUT_03_DELAY:
	case MODBUS_REG_INPUT_04_DELAY:
	case MODBUS_REG_INPUT_05_DELAY:
	case MODBUS_REG_INPUT_06_DELAY:
	case MODBUS_REG_INPUT_07_DELAY:
	case MODBUS_REG_INPUT_08_DELAY:
	case MODBUS_REG_INPUT_09_DELAY:
	case MODBUS_REG_INPUT_10_DELAY:
	case MODBUS_REG_INPUT_11_DELAY:
	case MODBUS_REG_INPUT_12_DELAY:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_INPUT_01_DELAY;

			if( val <= SYS_PANIC_DELAY_MAX )
			{
				config.input[j].delay = val;

				status = saveConfig();
			}
		}
		break;
	case MODBUS_REG_INPUT_01_ENABLE:
	case MODBUS_REG_INPUT_02_ENABLE:
	case MODBUS_REG_INPUT_03_ENABLE:
	case MODBUS_REG_INPUT_04_ENABLE:
	case MODBUS_REG_INPUT_05_ENABLE:
	case MODBUS_REG_INPUT_06_ENABLE:
	case MODBUS_REG_INPUT_07_ENABLE:
	case MODBUS_REG_INPUT_08_ENABLE:
	case MODBUS_REG_INPUT_09_ENABLE:
	case MODBUS_REG_INPUT_10_ENABLE:
	case MODBUS_REG_INPUT_11_ENABLE:
	case MODBUS_REG_INPUT_12_ENABLE:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_INPUT_01_ENABLE;

			config.input[j].enabled = val;

			status = saveConfig();
		}
		break;
	case MODBUS_REG_OUTPUT_01_TYPE:
	case MODBUS_REG_OUTPUT_02_TYPE:
	case MODBUS_REG_OUTPUT_03_TYPE:
	case MODBUS_REG_OUTPUT_04_TYPE:
	case MODBUS_REG_OUTPUT_05_TYPE:
	case MODBUS_REG_OUTPUT_06_TYPE:
	case MODBUS_REG_OUTPUT_07_TYPE:
	case MODBUS_REG_OUTPUT_08_TYPE:
	case MODBUS_REG_OUTPUT_09_TYPE:
	case MODBUS_REG_OUTPUT_10_TYPE:
	case MODBUS_REG_OUTPUT_11_TYPE:
	case MODBUS_REG_OUTPUT_12_TYPE:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_OUTPUT_01_TYPE;

			if( val < OUTPUT_NUM_TYPES )
			{
				config.output[j].type = static_cast<output_type_t>(val);

				status = saveConfig();
			}
		}
		break;
	case MODBUS_REG_OUTPUT_01_PULSE:
	case MODBUS_REG_OUTPUT_02_PULSE:
	case MODBUS_REG_OUTPUT_03_PULSE:
	case MODBUS_REG_OUTPUT_04_PULSE:
	case MODBUS_REG_OUTPUT_05_PULSE:
	case MODBUS_REG_OUTPUT_06_PULSE:
	case MODBUS_REG_OUTPUT_07_PULSE:
	case MODBUS_REG_OUTPUT_08_PULSE:
	case MODBUS_REG_OUTPUT_09_PULSE:
	case MODBUS_REG_OUTPUT_10_PULSE:
	case MODBUS_REG_OUTPUT_11_PULSE:
	case MODBUS_REG_OUTPUT_12_PULSE:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_OUTPUT_01_PULSE;

			config.output[j].pulse = val;

			status = saveConfig();
		}
		break;
	case MODBUS_REG_OUTPUT_01_ENABLE:
	case MODBUS_REG_OUTPUT_02_ENABLE:
	case MODBUS_REG_OUTPUT_03_ENABLE:
	case MODBUS_REG_OUTPUT_04_ENABLE:
	case MODBUS_REG_OUTPUT_05_ENABLE:
	case MODBUS_REG_OUTPUT_06_ENABLE:
	case MODBUS_REG_OUTPUT_07_ENABLE:
	case MODBUS_REG_OUTPUT_08_ENABLE:
	case MODBUS_REG_OUTPUT_09_ENABLE:
	case MODBUS_REG_OUTPUT_10_ENABLE:
	case MODBUS_REG_OUTPUT_11_ENABLE:
	case MODBUS_REG_OUTPUT_12_ENABLE:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_OUTPUT_01_ENABLE;

			config.output[j].enabled = val;

			status = saveConfig();
		}
		break;
	case MODBUS_REG_MODE_01_MASK:
	case MODBUS_REG_MODE_02_MASK:
	case MODBUS_REG_MODE_03_MASK:
	case MODBUS_REG_MODE_04_MASK:
	case MODBUS_REG_MODE_05_MASK:
	case MODBUS_REG_MODE_06_MASK:
	case MODBUS_REG_MODE_07_MASK:
	case MODBUS_REG_MODE_08_MASK:
	case MODBUS_REG_MODE_09_MASK:
	case MODBUS_REG_MODE_10_MASK:
	case MODBUS_REG_MODE_11_MASK:
	case MODBUS_REG_MODE_12_MASK:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_MODE_01_MASK;

			config.mode[j].mask = val;

			status = saveConfig();
		}
		break;
	case MODBUS_REG_MODE_01_ARM_DELAY:
	case MODBUS_REG_MODE_02_ARM_DELAY:
	case MODBUS_REG_MODE_03_ARM_DELAY:
	case MODBUS_REG_MODE_04_ARM_DELAY:
	case MODBUS_REG_MODE_05_ARM_DELAY:
	case MODBUS_REG_MODE_06_ARM_DELAY:
	case MODBUS_REG_MODE_07_ARM_DELAY:
	case MODBUS_REG_MODE_08_ARM_DELAY:
	case MODBUS_REG_MODE_09_ARM_DELAY:
	case MODBUS_REG_MODE_10_ARM_DELAY:
	case MODBUS_REG_MODE_11_ARM_DELAY:
	case MODBUS_REG_MODE_12_ARM_DELAY:
		if( ! state.armed )
		{
			auto j = offset - MODBUS_REG_MODE_01_ARM_DELAY;

			config.mode[j].armed_delay_enable = val;

			status = saveConfig();
		}
		break;
	case MODBUS_NUM_REGS:
		break;
	}

	if( status == 0 )
		num_bytes = 6;
	else
		num_bytes = handleModbusException(buf, static_cast<modbus_exception_t>(status));

	return num_bytes;
}

/*
 * Reads the modbus packet in buf and creates a response in buf
 *
 * num_bytes should not include the CRC which will be added below
 *
 * Returns the response size (bytes) in buf or 0 on failure
 */
unsigned processModbusPacket(uint8_t* buf, unsigned num_bytes)
{
	int status = 0;

	if( num_bytes < 4 )
		return status;

	//check CRC
	uint16_t real_crc = getCRC16(buf, num_bytes-2);

	uint8_t  crc_hi = buf[ num_bytes - 1 ];
	uint8_t  crc_lo = buf[ num_bytes - 2 ];
	uint16_t crc = crc_hi << 8 | crc_lo;

	if( real_crc != crc )
		return status;

	//parse packet data
	uint8_t  slave_id = buf[0];
	uint8_t  func     = buf[1];
	uint16_t offset   = buf[2] << 8 | buf[3];

	//check slave id
	if( slave_id != 0 && slave_id != SYS_MODBUS_SLAVE_ID )
		return status;

	//generate response
	buf[0] = slave_id;
	buf[1] = func;

	switch( func )
	{
	case 3:
		if( num_bytes == 8 )
			status = handleModbusFunc3(buf, offset);
		break;
	case 6:
		if( num_bytes == 8 )
			status = handleModbusFunc6(buf, offset);
		break;
	default:
		status = handleModbusException(buf, MODBUS_EXCEPTION_BAD_FUNC);
	}

	//add crc
	crc = getCRC16(buf, status);
	buf[status++] = lowByte(crc);
	buf[status++] = highByte(crc);

	return status;
}

/*
 * Manage the Modbus interface (used by the display)
 */
void manageModbus()
{
	unsigned buf_size = 256;

	uint8_t buf[buf_size];

	unsigned num_bytes = readRS485(buf, buf_size);

	if( num_bytes > 0 )
	{
		printModbusDebugInfo("RX: 0x", buf, num_bytes);

		num_bytes = processModbusPacket(buf, num_bytes);

		if( num_bytes > 0 )
		{
			printModbusDebugInfo("TX: 0x", buf, num_bytes);

			sendRS485(buf, num_bytes);
		}
	}
}

/*
 * Manage the alarm state
 */
void manageAlarmState()
{
	//process the input values
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		auto& input_cnfg = config.input[i];

		if( ! input_cnfg.enabled )
			continue;

		bool enabled_in_mode = state.alarm_mask & 0x1 << i;
		if( ! enabled_in_mode )
			continue;

		bool input_gone_low  = state.input_val_prev[i] != 0 && state.input_val[i] == 0;
		bool input_gone_high = state.input_val_prev[i] == 0 && state.input_val[i] != 0;

		switch( input_cnfg.type )
		{
		case INPUT_SIGNAL:

			if( input_gone_high )
			{
				setPanicState(true);
				Serial.print("Panic: input signal ");
				Serial.println(i+1);
			}
			break;
		case INPUT_SENSOR_MOTION:
		case INPUT_SENSOR_CONTACT_DOOR:
		case INPUT_SENSOR_CONTACT_WINDOW:
			if( input_gone_low )
			{
				if( state.armed )
				{
					if( input_cnfg.delay )
					{
						if( state.panic == false && state.panic_delay_count == 0 )
						{
							state.panic_delay_start = millis();
							state.panic_delay_count = input_cnfg.delay;
						}
					}
					else
					{
						setPanicState(true);
						Serial.print("Panic: ");
					}
				}

				Serial.print("activity at sensor ");
				Serial.println(i+1);
			}
			break;
		case INPUT_NUM_TYPES:
			break;
		}
	}

	//handle delayed panics
	if( state.panic_delay_count )
	{
		if( millis() - state.panic_delay_start > 1000 )
		{
			state.panic_delay_start += 1000;

			state.panic_delay_count--;

			if( state.panic_delay_count == 0 )
			{
				setPanicState(true);
				Serial.println("panic-delay: ALARM");
			}
			else
			{
				Serial.print("panic-delay: ");
				Serial.println(state.panic_delay_count);
			}
		}
	}

	//handle panic timeout
	if( state.panic )
	{
		uint32_t timeout_period = 1000;
		timeout_period *= 60 * 15;

		if( millis() - state.panic_start > timeout_period )
		{
			setPanicState(false);
			Serial.println("panic-state timed out");
		}
	}

	//handle delayed arming
	if( state.armed_delay_count )
	{
		if( millis() - state.armed_delay_start > 1000 )
		{
			state.armed_delay_start += 1000;

			state.armed_delay_count--;

			if( state.armed_delay_count == 0 )
			{
				state.armed = true;
				Serial.println("System Armed");
			}
			else
			{
				Serial.print("armed-delay: ");
				Serial.println(state.armed_delay_count);
			}
		}
	}

	//process the output values
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
	{
		auto& output_cnfg = config.output[i];

		if( ! output_cnfg.enabled )
		{
			setOutput(i, false);
			continue;
		}

		bool value = false;

		switch( output_cnfg.type )
		{
		case OUTPUT_ARMED:
			value = state.armed;
			break;
		case OUTPUT_PANIC:
			value = state.panic;
			break;
		case OUTPUT_DELAYED_ARMED:
			value = state.armed_delay_count;
			break;
		case OUTPUT_DELAYED_PANIC:
			value = state.panic_delay_count;
			break;
		case OUTPUT_NUM_TYPES:
			break;
		}

		if( output_cnfg.pulse )
		{
			if( value )
				value = millis() % 3000 > 1500;
		}

		setOutput(i, value);
	}

	//handle authentication
	if( state.auth )
	{
		uint32_t timeout_ms = SYS_AUTH_TIMEOUT * 1000;

		if( millis() - state.auth_tm > timeout_ms )
			state.auth = false;
	}
}

/*
 * Alarm System Setup
 */
void setup()
{
	setupInputs();
	setupOutputs();

	state.armed      = false;
	state.panic      = false;
	state.alarm_mode = 0;
	state.alarm_mask = 0xFFFF;

	Serial.begin(9600);
	Serial.println("OpenHouseAlarm I/O module starting up");

	int status = loadConfig();
	if( status != 0 )
		Serial.println("failed to load config");

	setupRS485(57600);
}

/*
 * Alarm System loop
 */
void loop()
{
	readAllInputs();

	manageAlarmState();

	manageModbus();

	delay(10);
}
