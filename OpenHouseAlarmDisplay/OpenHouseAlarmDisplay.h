/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm Display module - System Definitions
 */
#pragma once

//global includes
#include "../common/OpenHouseAlarm.h"


//global definitions
#define     SYS_RS485_DEV         "/dev/ttyS5"
#define     MIN_PIN_CHARS         3
#define     MAX_PIN_CHARS         9

struct disp_state_t                               //display state struct
{
	uint64_t   enable_start;                          //time the screen was turned on (ms)
    uint32_t   enable_duration;                       //time to keep the screen turned on (ms)
	bool       screen_on;                             //true if the display is currently on
};

//global variables
extern state_t     state;
extern config_t    config;
