#include "CommunicationTask.h"
#include "OpenHouseAlarmDisplay.h"
#include "Utils.h"

#include <fcntl.h>
#include <gpiod.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include <QDebug>


struct user_request_t                             //user request struct
{
	bool            done;                             //has the request finished
	uint16_t        reg;                              //register offset
	uint16_t        val;                              //value read/sent
};

struct rs485_port_t                               //RS485 port struct
{
	int fd;                                           //file descriptor for UART
	struct gpiod_line* gpio_de;                       //handle for DE gpio line
	struct gpiod_line* gpio_re;                       //handle for RE gpio line
};


//task global variables
user_request_t user_req;


/*
 * Returns the high byte in val
 */
uint8_t highByte(uint16_t val)
{
	return val >> 8;
}

/*
 * Returns the low byte in val
 */
uint8_t lowByte(uint16_t val)
{
	return val & 0xFF;
}

/*
 * Returns the number of (base 10) digits in val
 *
 * e.g. 0 => 1, 66 => 2, 999 => 3
 */
unsigned numDigits(unsigned val)
{
	unsigned digits = 0;

	if( val == 0 )
	{
		digits = 1;
	}
	else
	{
		while(val)
		{
			val /= 10;
			digits++;
		}
	}

	return digits;
}

/*
 * Print a modbus packet to the serial port
 */
void printModbusDebugInfo(const char* label, uint8_t* buf, unsigned num_bytes)
{
	if( num_bytes > 30 )
		num_bytes = 30;

	unsigned buf_size = (num_bytes*2) + 1;

	char str[buf_size];

	bufToHex(str, buf, num_bytes);

	qDebug() << label << str;
}

/*
 * Setup an output GPIO
 *
 * Returns a valid pointer to a gpio-line struct on success
 */
struct gpiod_line* setupOutputGPIO(const char* chip_name, unsigned line_offset, const char* line_desc)
{
	struct gpiod_chip* chip = gpiod_chip_open_by_name(chip_name);
	if( ! chip )
		return nullptr;

	struct gpiod_line* line = gpiod_chip_get_line(chip, line_offset);
	if( ! line )
	{
		gpiod_chip_close(chip);
		return nullptr;
	}

	int status = gpiod_line_request_output(line, line_desc, 0);

	if( status )
	{
		gpiod_chip_close(chip);
		return nullptr;
	}

	return line;
}

/*
 * Set up the com port
 *
 * Returns 0 on success
 */
int setupComPort(int fd, int baud)
{
	struct termios cnfg;

	int status = tcgetattr(fd, &cnfg);

	if( status == 0 )
	{
		//disable input & output processing
		cnfg.c_iflag &= ~( BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
		cnfg.c_iflag |= IGNBRK;
		cnfg.c_oflag = 0;

		//disable line processing
		cnfg.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

		//disable character processing
		cnfg.c_cflag &= ~(CSIZE | PARENB);
		cnfg.c_cflag |= CS8;

		//no minimum number of chars to read
		cnfg.c_cc[VMIN]  = 0;

		//no timeout
		cnfg.c_cc[VTIME] = 0;

		//set baud rate
		status += cfsetispeed(&cnfg, baud);
		status += cfsetospeed(&cnfg, baud);

		//apply config
		status += tcsetattr(fd, TCSANOW, &cnfg);
	}

	return status;
}

/*
 * Setup the passed UART
 *
 * Returns a valid file descriptor on success (>=0)
 */
int setupUART(const char* dev)
{
	int fd = open(dev, O_RDWR | O_NOCTTY);

	if( fd >= 0 )
	{
		int status = setupComPort(fd, B57600);

		if( status != 0 )
		{
			close(fd);
			fd = status;
		}
	}

	return fd;
}

/*
 * Setup the RS485 port
 *
 * Returns 0 on success
 */
int setupRS485(rs485_port_t& port)
{
	auto gpio_de = setupOutputGPIO("gpiochip0", 273, "rs485");
	auto gpio_re = setupOutputGPIO("gpiochip0", 272, "rs485");
	
	if( gpio_de && gpio_re )
	{
		//setup uart
		int fd = setupUART(SYS_RS485_DEV);

		if( fd >= 0 )
		{
			port.fd      = fd;
			port.gpio_de = gpio_de;
			port.gpio_re = gpio_re;
			return 0;
		}
	}

	return -1;
}

/*
 * Send num_bytes from buf on the RS485 port
 *
 * Returns the number of bytes sent
 */
int sendRS485(rs485_port_t& port, uint8_t* buf, unsigned num_bytes)
{
	gpiod_line_set_value(port.gpio_de, 1);
	gpiod_line_set_value(port.gpio_re, 1);

	int status = write(port.fd, buf, num_bytes);

	sleepMS(5);

	gpiod_line_set_value(port.gpio_re, 0);
	gpiod_line_set_value(port.gpio_de, 0);

	return status;
}

/*
 * Sends the modbus packet in buf and reads the response into buf
 *
 * num_bytes should not include the CRC which will be added below
 *
 * Returns the response size (bytes) in buf or 0 on failure
 */
unsigned processModbusPacket(rs485_port_t& port, uint8_t* buf, unsigned num_bytes)
{
	int status = 0;

	if( num_bytes < 6 )
		return status;

	//add CRC
	uint16_t crc = getModbusCRC(buf, num_bytes);
	buf[num_bytes++] = lowByte(crc);
	buf[num_bytes++] = highByte(crc);

	//flush buffers
	status = tcflush(port.fd, TCIOFLUSH);
	if( status )
		return 0;

	//send packet
	status = sendRS485(port, buf, num_bytes);

	printModbusDebugInfo("TX: 0x", buf, status);

	if( status != int(num_bytes) )
		return 0;

	//wait up to 250ms for a response
	status = waitForData(port.fd, 250);

	//read the response
	status = 0;
	while( true )
	{
		auto max_bytes = 250-status;

		if( max_bytes == 0 )
			break;

		status += read(port.fd, buf+status, max_bytes);

		//wait up to 10ms for more data
		if( ! waitForData(port.fd,10) )
			break;
	}

	printModbusDebugInfo("RX: 0x", buf, status);

	if( status >= 5 )
	{
		//check CRC
		crc = getModbusCRC(buf, status-2);
		uint8_t  crc_hi = buf[ status - 1 ];
		uint8_t  crc_lo = buf[ status - 2 ];
		uint16_t packet_crc = crc_hi << 8 | crc_lo;

		if( packet_crc != crc )
			status = 0;
	}
	else
	{
		status = 0;
	}

	return status;
}

/*
 * Read the state data from IO module
 *
 * Returns 0 on success
 */
int getStateData(rs485_port_t& port)
{
	uint8_t buf[256];

	int offset = MODBUS_REG_ARMED;
	int count  = 7;

	buf[0] = SYS_MODBUS_SLAVE_ID;
	buf[1] = 3;
	buf[2] = highByte(offset);
	buf[3] = lowByte(offset);
	buf[4] = highByte(count);
	buf[5] = lowByte(count);

	int num_bytes = 6;

	int status = processModbusPacket(port, buf, num_bytes);

	int expected_bytes = 5 + 2 * count;

	if( status == expected_bytes )
	{
		int i = 3;

		state.armed             = buf[i] << 8 | buf[i+1]; i+=2;
		state.armed_delay_count = buf[i] << 8 | buf[i+1]; i+=2;
		state.panic             = buf[i] << 8 | buf[i+1]; i+=2;
		state.panic_delay_count = buf[i] << 8 | buf[i+1]; i+=2;
		state.alarm_mode        = buf[i] << 8 | buf[i+1]; i+=2;
		state.alarm_mask        = buf[i] << 8 | buf[i+1]; i+=2;
		state.auth              = buf[i] << 8 | buf[i+1]; i+=2;

		status = 0;
	}

	return status;
}

/*
 * Read the config data from IO module
 *
 * Returns 0 on success
 */
int getConfigData(rs485_port_t& port)
{
	uint8_t buf[256];

	int offset = MODBUS_REG_INPUT_01_TYPE;
	int count  = MODBUS_NUM_REGS - MODBUS_REG_INPUT_01_TYPE;

	buf[0] = SYS_MODBUS_SLAVE_ID;
	buf[1] = 3;
	buf[2] = highByte(offset);
	buf[3] = lowByte(offset);
	buf[4] = highByte(count);
	buf[5] = lowByte(count);

	int num_bytes = 6;

	int status = processModbusPacket(port, buf, num_bytes);

	int expected_bytes = 5 + 2 * count;

	if( status == expected_bytes )
	{
		int i = 3;

		for(int j=0; j<SYS_NUM_INPUTS; j++)
		{
			uint16_t val = buf[i] << 8 | buf[i+1];
			i+=2;
			config.input[j].type = static_cast<input_type_t>( val );
		}

		for(int j=0; j<SYS_NUM_INPUTS; j++)
		{
			config.input[j].delay = buf[i] << 8 | buf[i+1];
			i+=2;
		}

		for(int j=0; j<SYS_NUM_INPUTS; j++)
		{
			config.input[j].enabled = buf[i] << 8 | buf[i+1];
			i+=2;
		}

		for(int j=0; j<SYS_NUM_OUTPUTS; j++)
		{
			uint16_t val = buf[i] << 8 | buf[i+1];
			i+=2;
			config.output[j].type = static_cast<output_type_t>( val );
		}

		for(int j=0; j<SYS_NUM_OUTPUTS; j++)
		{
			config.output[j].pulse = buf[i] << 8 | buf[i+1];
			i+=2;
		}

		for(int j=0; j<SYS_NUM_OUTPUTS; j++)
		{
			config.output[j].enabled = buf[i] << 8 | buf[i+1];
			i+=2;
		}

		for(int j=0; j<SYS_NUM_MODES; j++)
		{
			config.mode[j].mask = buf[i] << 8 | buf[i+1];
			i+=2;
		}

		for(int j=0; j<SYS_NUM_MODES; j++)
		{
			config.mode[j].armed_delay_enable = buf[i] << 8 | buf[i+1];
			i+=2;
		}

		status = 0;
	}
	else
	{
		status = -1;
	}

	return status;
}

/*
 * Write a modbus register
 *
 * Returns 0 on success
 */
int writeModbusRegister(rs485_port_t& port, uint16_t offset, uint16_t val)
{
	uint8_t buf[256];

	buf[0] = SYS_MODBUS_SLAVE_ID;
	buf[1] = 6;
	buf[2] = highByte(offset);
	buf[3] = lowByte(offset);
	buf[4] = highByte(val);
	buf[5] = lowByte(val);

	int num_bytes = 6;

	int status = processModbusPacket(port, buf, num_bytes);

	if( status == 8 )
		status = 0;

	return status;
}

/*
 * Handle any pending write request from the user
 *
 * Returns 0 on success
 */
int handleUserRequest(rs485_port_t& port)
{
	int status = 0;

	if( ! user_req.done )
	{
		status = writeModbusRegister(port, user_req.reg, user_req.val);

		user_req.done = true;
	}

	return status;
}

/*
 * Send an authentication request to the IO module
 */
void doAuthRequest(unsigned pincode)
{
	auto digits = numDigits(pincode);

	if( pincode == 0 )
		digits = MIN_PIN_CHARS;

	if( digits < MIN_PIN_CHARS || digits > MAX_PIN_CHARS )
	{
		qDebug() << "doAuthRequest: invalid pin: " << pincode;
	}
	else
	{
		qDebug() << "doAuthRequest: " << pincode;

		user_req.done = false;
		user_req.reg  = MODBUS_REG_AUTH;
		user_req.val  = pincode & 0xFFFF; //fixme
	}
}

/*
 * Send an arming request to the IO module
 */
void doArmRequest(unsigned mode)
{
	qDebug() << "doArmRequest: mode: " << mode;

	if( user_req.done )
	{
		user_req.done = false;
		user_req.reg  = MODBUS_REG_ARMED;
		user_req.val  = mode;
	}
}

/*
 * Manage Modbus communication with IO module
 */
void communicationTask()
{
	user_req = {};
	user_req.done = true;
	rs485_port_t port = {};
	bool reload_config = true;

	//setup
	while(true)
	{
		int status = setupRS485(port);

		if( status == 0 )
			break;

		qDebug() << "failed to setup RS485: " << status;

		sleepMS(200);
	}

	//loop
	while(true)
	{
		//read the IO module state
		int status = getStateData(port);
		if( status != 0 )
			qDebug() << "getStateData() returned status " << status;

		sleepMS(100);

		//read the IO module config
		if( reload_config )
		{
			status = getConfigData(port);

			if( status == 0 )
				reload_config = false;
			else
				qDebug() << "getConfigData() returned status " << status;
		}

		sleepMS(100);

		//handle any user request
		status = handleUserRequest(port);
		if( status != 0 )
			qDebug() << "handleUserRequest() returned status " << status;

		sleepMS(100);
	}
}
