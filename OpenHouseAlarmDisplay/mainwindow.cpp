#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "OpenHouseAlarmDisplay.h"
#include "CommunicationTask.h"

#include <QTimer>


enum screen_t {                                   //gui screens
	SCREEN_AUTH,
	SCREEN_HOME,
	SCREEN_CONFIG,
	NUM_SCREENS
};


/*
 * Returns true if the alarm is armed or arming
 */
bool isSystemArmed()
{
	return state.armed || state.armed_delay_count;
}

/*
 * Sets whether the passed widget should keep its size when hidden
 *
 * Returns 0 on success
 */
int retainSizeWhenHidden(QWidget* widget, bool keep_size)
{
	int status = -1;

	if( widget != nullptr )
	{
		auto sp = widget->sizePolicy();

		sp.setRetainSizeWhenHidden(keep_size);

		widget->setSizePolicy(sp);

		status = 0;
	}

	return status;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	//setup back button
	showBackButton(false);
	retainSizeWhenHidden(ui->back_button, true);

	updateDisplay();

	auto timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(updateDisplay()));
	connect(timer, SIGNAL(timeout()), this, SLOT(manageAuthStatus()));
	timer->start(750);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDisplay()
{
	QString s;

	s = state.armed ? "Armed" : "Disarmed";
	if( state.armed_delay_count )
		s = "Arming";
	ui->armed_status->setText(s);

	s = state.panic ? "Panic" : "Ok";
	ui->panic_status->setText(s);

	//disable unconfigured modes
	int i = 0;
	ui->button_mode_1->setEnabled(  config.mode[i++].mask );
	ui->button_mode_2->setEnabled(  config.mode[i++].mask );
	ui->button_mode_3->setEnabled(  config.mode[i++].mask );
	ui->button_mode_4->setEnabled(  config.mode[i++].mask );
	ui->button_mode_5->setEnabled(  config.mode[i++].mask );
	ui->button_mode_6->setEnabled(  config.mode[i++].mask );
	ui->button_mode_7->setEnabled(  config.mode[i++].mask );
	ui->button_mode_8->setEnabled(  config.mode[i++].mask );
	ui->button_mode_9->setEnabled(  config.mode[i++].mask );
	ui->button_mode_10->setEnabled( config.mode[i++].mask );
	ui->button_mode_11->setEnabled( config.mode[i++].mask );
	ui->button_mode_12->setEnabled( config.mode[i++].mask );
}

void MainWindow::manageAuthStatus()
{
	auto stack = ui->main_stack;

	auto index = stack->currentIndex();

	bool arming = state.armed_delay_count > 0;

	if( state.auth && ! arming )
	{
		if( index == SCREEN_AUTH )
			index = SCREEN_HOME;
	}
	else
	{
		index = SCREEN_AUTH;
		showBackButton(false);
	}

	stack->setCurrentIndex(index);
}

void MainWindow::doKeyPress(int key)
{
	if( key < 10 )
	{
		auto label = ui->keypad_line_edit;

		auto s = label->text();

		if( s.size() < MAX_PIN_CHARS )
		{
			s += QString::number(key);

			label->setText(s);
		}
	}
}

void MainWindow::on_keypad_btn_9_clicked()
{
	doKeyPress(9);
}

void MainWindow::on_keypad_btn_8_clicked()
{
	doKeyPress(8);
}

void MainWindow::on_keypad_btn_7_clicked()
{
	doKeyPress(7);
}

void MainWindow::on_keypad_btn_6_clicked()
{
	doKeyPress(6);
}

void MainWindow::on_keypad_btn_5_clicked()
{
	doKeyPress(5);
}

void MainWindow::on_keypad_btn_4_clicked()
{
	doKeyPress(4);
}

void MainWindow::on_keypad_btn_3_clicked()
{
	doKeyPress(3);
}

void MainWindow::on_keypad_btn_2_clicked()
{
	doKeyPress(2);
}

void MainWindow::on_keypad_btn_1_clicked()
{
	doKeyPress(1);
}

void MainWindow::on_keypad_btn_0_clicked()
{
	doKeyPress(0);
}

void MainWindow::on_keypad_btn_ok_clicked()
{
	auto label = ui->keypad_line_edit;

	auto s = label->text();

	if( s.size() > 0 )
		doAuthRequest( s.toInt() );

	label->setText("");
}

void MainWindow::on_keypad_btn_cancel_clicked()
{
	ui->keypad_line_edit->setText("");
}

void MainWindow::on_button_mode_1_clicked()
{
	doArmRequest(0);
}

void MainWindow::on_button_mode_2_clicked()
{
	doArmRequest(1);
}

void MainWindow::on_button_mode_3_clicked()
{
	doArmRequest(2);
}

void MainWindow::on_button_mode_4_clicked()
{
	doArmRequest(3);
}

void MainWindow::on_button_mode_5_clicked()
{
	doArmRequest(4);
}

void MainWindow::on_button_mode_6_clicked()
{
	doArmRequest(5);
}

void MainWindow::on_button_mode_7_clicked()
{
	doArmRequest(6);
}

void MainWindow::on_button_mode_8_clicked()
{
	doArmRequest(7);
}

void MainWindow::on_button_mode_9_clicked()
{
	doArmRequest(8);
}

void MainWindow::on_button_mode_10_clicked()
{
	doArmRequest(9);
}

void MainWindow::on_button_mode_11_clicked()
{
	doArmRequest(10);
}

void MainWindow::on_button_mode_12_clicked()
{
	doArmRequest(11);
}

void MainWindow::showBackButton(bool show)
{
	ui->back_button->setVisible(show);
}

void MainWindow::on_button_config_clicked()
{
	if( ! isSystemArmed() )
	{
		showBackButton(true);
		ui->main_stack->setCurrentIndex(SCREEN_CONFIG);
	}
}

void MainWindow::on_back_button_clicked()
{
	showBackButton(false);
	ui->main_stack->setCurrentIndex(SCREEN_HOME);
}
