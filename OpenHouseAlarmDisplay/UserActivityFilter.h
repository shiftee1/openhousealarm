/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm Display module - A custom filter with the following properties:
 *       1. Signals user activity
 *       2. Blocks touch events when the screen is off
 */
#pragma once

#include <QEvent>
#include <QObject>

class UserActivityFilter : public QObject
{
  Q_OBJECT

public:
	UserActivityFilter(bool* screen_on)
	{
		m_screen_on = screen_on;
	}

signals:
    void userActive();

protected:
	bool eventFilter(QObject *obj, QEvent *ev)
	{
		switch( ev->type() )
		{
			case QEvent::KeyPress:
			case QEvent::MouseMove:
			case QEvent::TouchBegin:
				emit userActive();
				if( ! *m_screen_on )
				{
					//block events while screen is off
					return true;
				}
				break;
			default:
				break;
		}

		return QObject::eventFilter(obj, ev);
	}

	bool* m_screen_on;
};

