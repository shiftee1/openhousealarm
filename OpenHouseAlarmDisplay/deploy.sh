#!/bin/sh
#
# Deploy code to display and compile
# If it builds then kill the existing process
#

USER=olimex
ADDR=$1

[ -z "$ADDR" ] && ADDR=openhousealarm.local

echo "copying files to target..."

rsync -a .. ${USER}@${ADDR}:/dev/shm/OpenHouseAlarm

ssh $SSH_OPTS $USER@$ADDR "
	cd /dev/shm/OpenHouseAlarm/OpenHouseAlarmDisplay
	qmake
	make clean
	make -j4

	[ $? -ne 0 ] && exit 1

	killall /usr/local/bin/OpenHouseAlarmDisplay
	cp OpenHouseAlarmDisplay /usr/local/bin/
	cp run_oha.sh            /usr/local/bin/
"

