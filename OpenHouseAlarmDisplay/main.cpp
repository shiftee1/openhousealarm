/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm Display module
 */

#include "mainwindow.h"
#include "OpenHouseAlarmDisplay.h"
#include "CommunicationTask.h"
#include "UserActivityFilter.h"
#include "Utils.h"

#include <thread>

#include <QApplication>

#define FILE_BACKGROUND_IMAGE  "/usr/local/share/openhousealarm/lake_yacht_blue.svg"

//global variables
state_t      state;                               //system state
config_t     config;                              //system configuration
disp_state_t disp_state;                          //display state


/*
 * Turn on the screen for a limited time
 */
void enableBacklight(unsigned enable_ms)
{
	disp_state.enable_start    = millis();
	disp_state.enable_duration = enable_ms;
}

/*
 * Backlight management task
 */
int backlightTask()
{
	const char* backlight_dev = "/sys/class/graphics/fb0/blank";

	FILE* fd = nullptr;

	disp_state.screen_on       = true;
	disp_state.enable_duration = 60 * 1000;

	while(true)
	{
		if( fd == nullptr )
		{
			fd = fopen(backlight_dev, "w");
			if( fd )
				printf("Opened backlight handle\n");
		}
		else
		{
			if( millis() - disp_state.enable_start > disp_state.enable_duration )
			{
				if( disp_state.screen_on )
				{
					fprintf(fd, "1");
					fflush(fd);
					disp_state.screen_on = false;
					printf("Disabled screen\n");
				}
			}
			else
			{
				if( ! disp_state.screen_on )
				{
					fprintf(fd, "0");
					fflush(fd);
					disp_state.screen_on = true;
					printf("Enabled screen\n");
				}
			}
		}

		sleepMS(250);
	}
}

/*
 * Set the background image for the main window
 */
void setBackgroundImage(MainWindow& w, const char* image)
{
	auto css = QString("QWidget#my_main_window { border-image:url(%1) 0 0 0 0 stretch stretch }").arg(image);
	w.setObjectName("my_main_window");
	w.setStyleSheet(css);
}

/*
 * Main Function
 */
int main(int argc, char *argv[])
{
	std::thread com_task(communicationTask);
	std::thread bl_task(backlightTask);

	QApplication a(argc, argv);

	UserActivityFilter filter(&disp_state.screen_on);
	QObject::connect( &filter, &UserActivityFilter::userActive, [=] () {
		enableBacklight(30 * 1000);
	} );
	a.installEventFilter(&filter);

	MainWindow w;
	setBackgroundImage(w, FILE_BACKGROUND_IMAGE);
	w.showFullScreen();

	return a.exec();
}
