#-------------------------------------------------
#
# Project created by QtCreator 2019-03-05T23:37:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OpenHouseAlarmDisplay
TEMPLATE = app

CONFIG += C++17

SOURCES += main.cpp\
        mainwindow.cpp \
    CommunicationTask.cpp \
    Utils.cpp

HEADERS  += mainwindow.h \
    OpenHouseAlarmDisplay.h \
    CommunicationTask.h \
    UserActivityFilter.h \
    Utils.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -Wextra -Wshadow

LIBS += -lgpiod
