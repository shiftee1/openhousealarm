#include "Utils.h"

#include <chrono>
#include <cstdio>
#include <thread>
#include <poll.h>

//program start time for millis()
static std::chrono::steady_clock::time_point prog_start_tm = std::chrono::steady_clock::now();

/*
 * Returns the number of milliseconds since program start
 */
uint64_t millis()
{
	auto now    = std::chrono::steady_clock::now();
	auto dur    = now - prog_start_tm;
	auto dur_ms = std::chrono::duration_cast<std::chrono::milliseconds>(dur);
	return dur_ms.count();
}
/*
 * Sleeps the calling thread for count milliseconds
 */
void sleepMS(unsigned count)
{
	std::this_thread::sleep_for( std::chrono::milliseconds(count) );
}

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t getCRC16(uint8_t* buf, unsigned num_bytes, uint16_t poly, uint16_t init_val)
{
	uint16_t crc = init_val;

	for(unsigned i=0; i<num_bytes; i++)
	{
		crc ^= buf[i];

		for(int j=0; j<8; j++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ poly;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Calculates the Modbus CRC for num_bytes of buf
 */
uint16_t getModbusCRC(uint8_t* buf, unsigned num_bytes)
{
	return getCRC16(buf, num_bytes, 0xA001, 0xFFFF);
}

/*
 * Formats the data in src buffer as a hex string
 *
 * Returns the number of chars
 */
int bufToHex(char* dest, uint8_t* src, unsigned num_bytes)
{
	//null terminate if num_bytes == 0
	dest[0] = 0;

	int chars = 0;

	for(unsigned i=0; i<num_bytes; i++)
		chars += sprintf(dest+i*2, "%02X", src[i]);

	return chars;
}

/*
 * Block until data arrives or timeout expires
 *
 * Returns:
 *      0 on timeout
 *     >0 if data is available
 *     -1 on error
 */
int waitForData(int fd, int timeout_ms)
{
 	struct pollfd fds[1];

	fds[0].fd = fd;
	fds[0].events = POLLIN;

	return poll(fds, 1, timeout_ms);
}

