/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm Display module - UI code
 */
#pragma once

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void updateDisplay();
	void manageAuthStatus();

private slots:
	void on_keypad_btn_9_clicked();
	void on_keypad_btn_8_clicked();
	void on_keypad_btn_7_clicked();
	void on_keypad_btn_6_clicked();
	void on_keypad_btn_5_clicked();
	void on_keypad_btn_4_clicked();
	void on_keypad_btn_3_clicked();
	void on_keypad_btn_2_clicked();
	void on_keypad_btn_1_clicked();
	void on_keypad_btn_0_clicked();
	void on_keypad_btn_ok_clicked();
	void on_keypad_btn_cancel_clicked();

	void on_button_mode_1_clicked();
	void on_button_mode_2_clicked();
	void on_button_mode_3_clicked();
	void on_button_mode_4_clicked();
	void on_button_mode_5_clicked();
	void on_button_mode_6_clicked();
	void on_button_mode_7_clicked();
	void on_button_mode_8_clicked();
	void on_button_mode_9_clicked();
	void on_button_mode_10_clicked();
	void on_button_mode_11_clicked();
	void on_button_mode_12_clicked();

	void showBackButton(bool show);
	void on_back_button_clicked();
	void on_button_config_clicked();

private:
	void doKeyPress(int key);
	Ui::MainWindow *ui;
};
