/*
 * Copyright (C) 2021
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * OpenHouseAlarm Display module - Utility functions
 */
#pragma once

#include <stdint.h>

uint64_t millis();
void     sleepMS(unsigned count);

uint16_t getCRC16(uint8_t* buf, unsigned num_bytes, uint16_t poly, uint16_t init_val);
uint16_t getModbusCRC(uint8_t* buf, unsigned num_bytes);
int      bufToHex(char* dest, uint8_t* src, unsigned num_bytes);
int      waitForData(int fd, int timeout_ms);

